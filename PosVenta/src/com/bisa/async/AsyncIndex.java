package com.bisa.async;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bisa.fragments.PedidosFragment;
import com.bisa.json.JSONParser;
import com.bisa.posventa.Index;
import com.bisa.posventa.R;

public class AsyncIndex extends AsyncTask<String, String, JSONObject> {

	// Variables globales
	Index contexto;
	JSONObject json;
	JSONArray filtros;
	JSONArray filtros2;
	ListView lvPedidos;
	int size; // Tamano del arreglo de filtro 2, para comproar que si trajo
				// valores

	// Constantes
	private final static String URL = "http://renjiz.byethost13.com/services/pos_obtenerIndexFiltros.php";
	private static final String TAG_ITEMS = "items";
	private static final String TAG_ITEMS2 = "items2";
	private static final String NOMBRE = "cliente_nombre";
	private static final String ID_PEDIDO = "id_pedido";
	private static final String FECHA = "fecha_pedido";
	private static final String TOTAL = "total";

	// Elementos de la actividad y sus fragments
	EditText clienteCampo;
	TextView clienteNombrePedido;
	TextView clienteNombreCartera;

	// Construcotres
	public AsyncIndex(Index contexto) {
		this.contexto = contexto;

		clienteCampo = (EditText) contexto
				.findViewById(R.id.IndexEditTextCliente);
		clienteNombrePedido = (TextView) contexto
				.findViewById(R.id.fragmentPedidoTextViewTitulo);
		clienteNombreCartera = (TextView) contexto
				.findViewById(R.id.fragmentTextViewTitulo);

		//
	}

	// Estructura de datos para guardar valores JSON
	HashMap<String, String> map1 = new HashMap<String, String>();
	HashMap<String, String> map2 = new HashMap<String, String>();
	// Aqui se guardaran todos los hashmap creados
	ArrayList<HashMap<String, String>> filtrosList = new ArrayList<HashMap<String, String>>();

	// Barra de loading
	ProgressDialog progressDialog;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(contexto, "Buscando",
				"Por favor espere un momento...", true);
		progressDialog.setCancelable(true);
		progressDialog.setIndeterminate(true);

		// do initialization of required objects objects here

	}

	@Override
	protected JSONObject doInBackground(String... arg0) {

		try {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			String cliente = clienteCampo.getText().toString();
			params.add(new BasicNameValuePair("cliente", cliente));

			json = jParser.makeHttpRequest(URL, "POST", params);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@Override
	protected void onPostExecute(JSONObject json) {

		try {
			// Obteniendo datos del arreglo filtros
			filtros = json.getJSONArray(TAG_ITEMS);
			// Obteniendo datos del arreglo filtros2
			filtros2 = json.getJSONArray(TAG_ITEMS2);
			// Se chequea si trajo valores la consulta
			int cantidad = filtros.length();
			if (cantidad == 0) {

				Toast.makeText(contexto, "NO SE ENCONTRO NINGUN CLIENTE!",
						Toast.LENGTH_LONG).show();
			} else {
				// looping through All Contacts
				for (int i = 0; i < filtros.length(); i++) {

					JSONObject c = filtros.getJSONObject(i);

					// Storing each json item in variable
					String nombre = c.getString(NOMBRE);

					// adding each child node to HashMap key => value
					map1.put(NOMBRE, nombre);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			// Sacando valores para lista
			size = filtros2.length();
			Log.d("tamano array", String.valueOf(size));
			if (size != 0) {
				for (int x = 0; x < filtros2.length(); x++) {

					JSONObject f = filtros2.getJSONObject(x);

					String id = f.getString(ID_PEDIDO);
					String fecha = f.getString(FECHA);
					String total = f.getString(TOTAL);
					map2.put(ID_PEDIDO, id);
					map2.put(FECHA, fecha);
					map2.put(TOTAL, total);

					filtrosList.add(map2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Terminar el progressDialog
		progressDialog.dismiss();

		// Actualizar UI
		clienteNombrePedido.setText(map1.get(NOMBRE));
		clienteNombreCartera.setText(map1.get(NOMBRE));

		// Lista pedidos
		if (size != 0) {
			
			final ListAdapter adapter = new SimpleAdapter(contexto,
					filtrosList, R.layout.fragmento_pedido_list_item,
					new String[] { ID_PEDIDO, FECHA, TOTAL }, new int[] {
							R.id.fragmento_pedido_id,
							R.id.fragmento_pedido_fecha,
							R.id.fragmento_pedido_total });

			lvPedidos = (ListView) contexto
					.findViewById(R.id.fragmentPedidosListView);
			lvPedidos.setAdapter(adapter);
		} else {
			

			Toast.makeText(contexto, "NO SE ENCONTRO NINGUN PEDIDO!",
					Toast.LENGTH_LONG).show();
		}

	}

}
