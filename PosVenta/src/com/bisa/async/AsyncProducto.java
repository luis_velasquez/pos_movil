package com.bisa.async;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bisa.json.JSONParser;
import com.bisa.posventa.ProductoActivity;
import com.bisa.posventa.R;

public class AsyncProducto extends AsyncTask<String, String, JSONObject> {

	// Constantes y globales
	JSONObject json;
	ProductoActivity contexto;
	String codigoId;
	JSONArray productos;
	TextView editTextPv; //edit text del precio de venta
	EditText editTextDescripcion; // edit text de la descripcion
	EditText editTextCantidad;
	EditText editTextDescuento;
	String flag; // flag para saber si el asynctask es del buscar o del onchange
					// del spinner
	String nombre; // nombre del item del spinner
	int pos; // posicion del item del spinner
	private Spinner spinner1;

	// Estructura de datos para guardar valores JSON
	HashMap<String, String> map = new HashMap<String, String>();
	// Estructura de datos para guardar todos los HashMap creados
	ArrayList<HashMap<String, String>> listaMap = new ArrayList<HashMap<String, String>>();
	// Estructura de datos para los valores del spinner
	List<String> spinnerValues = new ArrayList<String>();
	List<String> preciosVentas = new ArrayList<String>();

	private static final String URL = "http://renjiz.byethost13.com/services/pos_obtenerProducto.php";
	private static final String TAG_ITEMS = "items";
	private static final String CODIGO = "producto_codigo";
	private static final String NOMBRE = "producto_nombre";
	private static final String DESCRIPCION = "producto_descripcion";
	private static final String PRECIO_VENTA = "unidad_medida_precio_venta";
	private static final String UNIDAD_MEDIDA = "unidad_medida_nombre";

	// Constructores para recibir el contexto de la actividad y otros parametros
	public AsyncProducto(ProductoActivity context, String id, String flag) {
		// TODO Auto-generated constructor stub
		contexto = context;
		codigoId = id;
		this.flag = flag;

		editTextPv = (TextView) contexto.findViewById(R.id.TextViewPv);
		editTextDescripcion = (EditText) contexto.findViewById(R.id.EditTextDescripcion);

	}

	public AsyncProducto(ProductoActivity context, String id, String flag,
			String nombre, int pos) {
		// TODO Auto-generated constructor stub
		contexto = context;
		codigoId = id;
		this.flag = flag;
		this.nombre = nombre;
		this.pos = pos;

		editTextPv = (TextView) contexto.findViewById(R.id.TextViewPv);
		editTextDescripcion = (EditText) contexto.findViewById(R.id.EditTextDescripcion);

	}

	public AsyncProducto(ProductoActivity context, String flag) {
		contexto = context;
		this.flag = flag;
	}

	// Barra de loading
	ProgressDialog progressDialog;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(contexto, "Buscando Producto",
				"Por favor espere un momento...", true);
		progressDialog.setCancelable(true);
		progressDialog.setIndeterminate(true);

		// do initialization of required objects objects here

	}

	// OPERACIONES EN BACKGROUND
	@Override
	protected JSONObject doInBackground(String... args) {

		try {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("id", codigoId));
			if (flag.equals("nada")) {
				params.add(new BasicNameValuePair("nombre", nombre));
			}
			// getting JSON string from URL
			json = jParser.makeHttpRequest(URL, "POST", params);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}

	@Override
	protected void onPostExecute(JSONObject json) {
		spinnerValues.add(" ");
		if (flag.equals("zero")) {
			try {
				// Getting Array of Contacts
				productos = json.getJSONArray(TAG_ITEMS);

				int cantidad = productos.length();
				if (cantidad == 0) {
					// TextView tv = (TextView) getActivity().findViewById(
					// R.id.mensaje);
					// tv.setVisibility(View.VISIBLE);
					// tv.setText("No tiene clientes actualmente.");
					Toast.makeText(contexto, "NO SE ENCONTRO NINGUN PRODUCTO!",
							Toast.LENGTH_LONG).show();
				} else {

					// looping through All Contacts
					for (int i = 0; i < productos.length(); i++) {
						JSONObject c = productos.getJSONObject(i);

						// Storing each json item in variable
						String nombre = c.getString(NOMBRE);
						String unidadMedida = c.getString(UNIDAD_MEDIDA);
						String precioVenta = c.getString(PRECIO_VENTA);
						String descripcion = c.getString(DESCRIPCION);

						// creating new HashMap

						// adding each child node to HashMap key => value
						map.put(NOMBRE, nombre);
						map.put(UNIDAD_MEDIDA, unidadMedida);
						map.put(PRECIO_VENTA, precioVenta);
						map.put(DESCRIPCION, descripcion);

						listaMap.add(map);
						spinnerValues.add(unidadMedida);
						preciosVentas.add(precioVenta);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} else {
			try {
				// Getting Array of Contacts
				productos = json.getJSONArray(TAG_ITEMS);

				int cantidad = productos.length();
				if (cantidad == 0) {
					// TextView tv = (TextView) getActivity().findViewById(
					// R.id.mensaje);
					// tv.setVisibility(View.VISIBLE);
					// tv.setText("No tiene clientes actualmente.");
					Toast.makeText(contexto,
							"NO SE ENCONTRO NINGUN PRODUCTO! xxx",
							Toast.LENGTH_LONG).show();
				} else {

					// looping through All Contacts
					for (int i = 0; i < productos.length(); i++) {
						JSONObject c = productos.getJSONObject(i);

						// Storing each json item in variable
						String nombre = c.getString(NOMBRE);
						String unidadMedida = c.getString(UNIDAD_MEDIDA);
						String precioVenta = c.getString(PRECIO_VENTA);
						String descripcion = c.getString(DESCRIPCION);

						// creating new HashMap

						// adding each child node to HashMap key => value
						map.put(NOMBRE, nombre);
						map.put(UNIDAD_MEDIDA, unidadMedida);
						map.put(PRECIO_VENTA, precioVenta);
						map.put(DESCRIPCION, descripcion);

						listaMap.add(map);
						spinnerValues.add(unidadMedida);
						preciosVentas.add(precioVenta);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// Terminar el progressDialog
		progressDialog.dismiss();
		String valor;
		//asignando valor segun flag
		if (flag.equals("zero")) {
			 valor = "0";
		} else {
			 valor = preciosVentas.get(0);
		}

		// Actualizar pantalla con datos del JSON
		
		//Precio de venta
		editTextPv.setText(valor);
		Log.d("PRECIO VENTA", valor);
		//Si flag es zero la peticion viene del boton buscar asi que hay que resetear el adaptador del spinner
		if (flag.equals("zero")) {
			// Llenar spinner
			spinner1 = (Spinner) contexto.findViewById(R.id.Spinner1);

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
					contexto, android.R.layout.simple_spinner_item,
					spinnerValues);
			dataAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner1.setAdapter(dataAdapter);
			
			
		} 
		//Vaciar campos de cantidad y descuento
		editTextCantidad = (EditText) contexto.findViewById(R.id.EditTextCantidad);
		editTextDescuento = (EditText) contexto.findViewById(R.id.EditTextDescuento);
		
		editTextCantidad.setText("");
		editTextDescuento.setText("");
		//Descripcion
		editTextDescripcion.setText(map.get(DESCRIPCION));

	}

} // fffhfhfh
