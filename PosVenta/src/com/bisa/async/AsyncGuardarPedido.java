package com.bisa.async;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.webkit.WebView.FindListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bisa.json.JSONParser;
import com.bisa.posventa.Index;
import com.bisa.posventa.ProductoActivity;
import com.bisa.posventa.R;

public class AsyncGuardarPedido extends AsyncTask<String, String, String> {

	// Constantes y globales
	JSONObject json;
	ProductoActivity contexto;
	String codigoCliente;
	JSONArray productos;
	TextView editTextPv; //edit text del precio de venta
	TextView fecha;
	EditText editTextDescripcion; // edit text de la descripcion
	EditText editTextCantidad;
	EditText editTextDescuento;
	TextView total;
	String flag; // flag para saber si el asynctask es del buscar o del onchange
					// del spinner
	String nombre; // nombre del item del spinner
	int pos; // posicion del item del spinner
	private Spinner spinner1;

	// Estructura de datos para guardar valores JSON
	HashMap<String, String> map = new HashMap<String, String>();
	// Estructura de datos para guardar todos los HashMap creados
	ArrayList<HashMap<String, String>> listaMap = new ArrayList<HashMap<String, String>>();
	// Estructura de datos para los valores del spinner
	List<String> spinnerValues = new ArrayList<String>();
	List<String> preciosVentas = new ArrayList<String>();

	private static final String URL = "http://renjiz.byethost13.com/services/pos_guardarPedido.php";
	private static final String TAG_ITEMS = "items";
	private static final String CODIGO = "producto_codigo";
	private static final String NOMBRE = "producto_nombre";
	private static final String DESCRIPCION = "producto_descripcion";
	private static final String PRECIO_VENTA = "unidad_medida_precio_venta";
	private static final String UNIDAD_MEDIDA = "unidad_medida_nombre";

	// Constructores para recibir el contexto de la actividad y otros parametros
	public AsyncGuardarPedido(ProductoActivity context, String codigo) {
		// TODO Auto-generated constructor stub
		this.contexto = context;
		this.codigoCliente = codigo;
		
        total = (TextView) contexto.findViewById(R.id.TextViewTotal);
		editTextPv = (TextView) contexto.findViewById(R.id.TextViewPv);
		editTextDescripcion = (EditText) contexto.findViewById(R.id.EditTextDescripcion);
		fecha = (TextView) contexto.findViewById(R.id.ProductoTextViewFecha);
		

	}

	// Barra de loading
	ProgressDialog progressDialog;

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressDialog = ProgressDialog.show(contexto, "Guardando Pedido",
				"Por favor espere un momento...", true);
		progressDialog.setCancelable(false);
		progressDialog.setIndeterminate(true);

		// do initialization of required objects objects here

	}

	// OPERACIONES EN BACKGROUND
	@Override
	protected String doInBackground(String... args) {

		try {
			// Creating JSON Parser instance
			JSONParser jParser = new JSONParser();

			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("codigoCliente", codigoCliente));
			params.add(new BasicNameValuePair("fecha", fecha.getText().toString()));
			params.add(new BasicNameValuePair("total", total.getText().toString()));
			params.add(new BasicNameValuePair("estado", "1"));
			
			// getting JSON string from URL
			json = jParser.makeHttpRequest(URL, "POST", params);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(String str) {
		// Terminar el progressDialog
		progressDialog.dismiss();
		
		Intent intent = new Intent(contexto, Index.class);
		contexto.startActivity(intent);
		contexto.finish();

	}

} // fffhfhfh
