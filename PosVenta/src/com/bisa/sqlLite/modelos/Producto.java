package com.bisa.sqlLite.modelos;

public class Producto {

	// Variables privadas
	int _id;
	String codigo;
	String descripcion;
	int cantidad;
	double total;
	int pedidoId;

	// Constructor vacio
	public Producto() {

	}

	// Constructor
	public Producto(int id, String codigo, String descripcion, int cantidad,
			double total, int pedidoId) {
		this._id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.total = total;
	}

	// Constructor
	public Producto(String codigo, String descripcion, int cantidad, double total, int pedidoId) {

		this.codigo = codigo;
		this.descripcion = descripcion;
		this.cantidad = cantidad;
		this.total = total;
	}
	
	//Getters y setters
	

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getPedidoId() {
		return pedidoId;
	}

	public void setPedidoId(int pedidoId) {
		this.pedidoId = pedidoId;
	}
}
