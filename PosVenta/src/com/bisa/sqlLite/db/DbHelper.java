package com.bisa.sqlLite.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.inputmethodservice.Keyboard.Key;

import com.bisa.posventa.Tabla1;
import com.bisa.sqlLite.modelos.Producto;

public class DbHelper extends SQLiteOpenHelper {

	//Globales
	long lastId;
	// Variables estaticas
	// Version de la bd
	private static final int DATABASE_VERSION = 3;

	// Database name
	private static final String DATABASE_NAME = "posVenta";

	// Nombre de la tabla producto
	private static final String TABLE_PRODUCTO = "producto";

	// Nombre de las columnas de tabla producto
	private static final String KEY_ID = "_id";
	private static final String KEY_CODIGO = "codigo";
	private static final String KEY_DESCRIPCION = "descripcion";
	private static final String KEY_cantidad = "cantidad";
	private static final String KEY_TOTAL = "total";
	private static final String KEY_PEDIDO_ID = "pedido_id";

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creando tablas
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_PRODUCTOS_TABLE = "CREATE TABLE " + TABLE_PRODUCTO + "("
				+ KEY_ID + " INTEGER PRIMARY KEY, " + KEY_CODIGO + " TEXT, "
				+ KEY_DESCRIPCION + " TEXT, " + KEY_cantidad + " INTEGER, "
				+ KEY_TOTAL + " REAL," + KEY_PEDIDO_ID + " INTEGER " + " )";
		db.execSQL(CREATE_PRODUCTOS_TABLE);
	}

	// Upgrading db
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop tabla antigua si existe
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTO);
		// Crear tabla nuevamente
		onCreate(db);
	}

	// ////////////////////////////////////////////////////////////////
	// Todas las operaciones CRUD

	// Agregando productos
	public void addProducto(Producto producto) {

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CODIGO, producto.getCodigo());
		values.put(KEY_DESCRIPCION, producto.getDescripcion());
		values.put(KEY_cantidad, producto.getCantidad());
		values.put(KEY_TOTAL, producto.getTotal());
		values.put(KEY_PEDIDO_ID, producto.getPedidoId());

		// Insertando fila
		db.insert(TABLE_PRODUCTO, null, values);
		db.close(); // Cerrando database connection
	}

	// Obteniendo un producto especifico
	public Producto getProducto(int id) {

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PRODUCTO, new String[] { KEY_ID,
				KEY_CODIGO, KEY_DESCRIPCION, KEY_cantidad, KEY_TOTAL,
				KEY_PEDIDO_ID }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if (cursor != null) {
			cursor.moveToFirst();
		}

		Producto producto = new Producto(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2),
				Integer.parseInt(cursor.getString(3)),
				Double.parseDouble(cursor.getString(4)),
				Integer.parseInt(cursor.getString(5)));

		// Regresar producto
		return producto;

	}
    //Obteniendo el ultimo producto
	public Producto getLastProducto() {

		SQLiteDatabase db = this.getReadableDatabase();
		
		String query = "SELECT ROWID from " + TABLE_PRODUCTO + " order by ROWID DESC limit 1";
		Cursor c = db.rawQuery(query, null);
		if (c != null && c.moveToFirst()) 
		{
		     lastId = c.getLong(0);
		//The 0 is the column index, we only have 1 column, so the index is 0
		}
		Cursor cursor = db.query(TABLE_PRODUCTO, new String[] { KEY_ID,
				KEY_CODIGO, KEY_DESCRIPCION, KEY_cantidad, KEY_TOTAL,
				KEY_PEDIDO_ID }, KEY_ID + "=?",
				new String[] { String.valueOf(lastId) }, null, null, null, null);

		if (cursor != null) {
			cursor.moveToFirst();
		}

		Producto producto = new Producto(Integer.parseInt(cursor.getString(0)),
				cursor.getString(1), cursor.getString(2),
				Integer.parseInt(cursor.getString(3)),
				Double.parseDouble(cursor.getString(4)),
				Integer.parseInt(cursor.getString(5)));

		// Regresar producto
		return producto;
	}

	// Obteniendo todos los productos
	public List<Producto> getAllProductos() {
		List<Producto> productoList = new ArrayList<Producto>();
		// Select All Query
		String selectQuery = "Select * from " + TABLE_PRODUCTO;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// Looping atravez de todas las filas y agregando a la lista
		if (cursor.moveToFirst()) {
			do {
				Producto producto = new Producto();
				producto.set_id(Integer.parseInt(cursor.getString(0)));
				producto.setCodigo(cursor.getString(1));
				producto.setDescripcion(cursor.getString(2));
				producto.setCantidad(Integer.parseInt(cursor.getString(3)));
				producto.setTotal(Double.parseDouble(cursor.getString(4)));
				producto.setPedidoId(Integer.parseInt(cursor.getString(5)));
				// Agregando producto a lista
				productoList.add(producto);
			} while (cursor.moveToNext());

		}
		// Retornar lista
		return productoList;
	}

	// Obteniendo el count de productos
	public int getProductosCount() {
		String countQuery = "SELECT * FROM " + TABLE_PRODUCTO;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// Retornar conteo
		return cursor.getCount();
	}

	// Uptading producto
	public int updateProducto(Producto producto) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_cantidad, producto.getCantidad());
		values.put(KEY_TOTAL, producto.getTotal());

		// Actualizando fila
		return db.update(TABLE_PRODUCTO, values, KEY_ID + " = ?",
				new String[] { String.valueOf(producto.get_id()) });
	}

	// Borrando un producto
	public void deleteProducto(Producto producto) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PRODUCTO, KEY_ID + " =?",
				new String[] { String.valueOf(producto.get_id()) });
		db.close();
	}
}