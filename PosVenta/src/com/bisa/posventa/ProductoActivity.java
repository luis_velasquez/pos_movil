package com.bisa.posventa;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bisa.async.AsyncProducto;
import com.bisa.modelos.CustomOnItemSelectedListener;
import com.bisa.modelos.InputFilterMinMax;
import com.bisa.posventa.R.drawable;
import com.bisa.sqlLite.db.DbHelper;
import com.bisa.sqlLite.modelos.Producto;

public class ProductoActivity extends Activity implements
		android.view.View.OnClickListener {

	// CONSTANTES
	String flag = "zero";
	private Spinner spinner1;
	int check = 0;

	// Base de datos
	DbHelper db;
	// CONSTANTES DE CAMPOS DEL FORM
	EditText cantidad;
	EditText descripcion;

	EditText descuento;
	EditText codigo;
	TextView precioVenta;
	TextView total;

	// CONSTANTES PARA SACAR EL TOTAL
	double cantidadTotal;
	double descuentoTotal;
	double precioVentaTotal;
	double totalT;

	// CONSTANTES DEL TABLELAYOUT
	TableLayout grid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_producto);

		// Instanciando helper
		db = new DbHelper(this);

		// Obteniendo acceso a los elementos del layout
		codigo = (EditText) findViewById(R.id.editTextCodigo);
		cantidad = (EditText) findViewById(R.id.EditTextCantidad);
		descripcion = (EditText) findViewById(R.id.EditTextDescripcion);

		descuento = (EditText) findViewById(R.id.EditTextDescuento);
		// Pasandole la clase con el filtro para poner un rango maximo y asi
		// evitar totales negativos
		descuento.setFilters(new InputFilter[] { new InputFilterMinMax("0",
				"100") });

		precioVenta = (TextView) findViewById(R.id.TextViewPv);
		total = (TextView) findViewById(R.id.TextViewTotal);

		// Probando acceder a childs de tabla de grid
		grid = (TableLayout) findViewById(R.id.tabla_grid); // ligando variable
															// a tabla
		TableRow row = (TableRow) grid.getChildAt(1); // accediendo a una fila
														// de la tabla
		View fila = (View) grid.getChildAt(1); // Obtener toda la fila para
												// hacerle click
		// Esto es para acceder a un child de la fila
		// TextView textView = (TextView) row.getChildAt(1);

		Button agregar = (Button) findViewById(R.id.ButtonAgregar);
		Button buscar = (Button) findViewById(R.id.buttonBuscar);
		// //////////////////////////////////////////////////////////////////////////////////
		// Agregando listeners

		cantidad.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				// descuento.setText("Cambio de texto!");
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				String cant = cantidad.getText().toString();
				String des = descuento.getText().toString();
				// Si descuento contiene numero realiza la operacion y las
				// casillas de descuento y cantidad no estan vacias
				if (cant.length() != 0 && des.length() != 0
						&& des.matches(".*\\d.*")) {
					total();
				} else {
					// Sustituimos el valor del total si no se cumple
					total.setText("Total");
				}
			}
		});

		descuento.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				// descuento.setText("Cambio de texto!");
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				String des = descuento.getText().toString();
				String cant = cantidad.getText().toString();
				// Si cantidad contiene numero realiza la operacion y las
				// casillas de descuento y cantidad no estan vacias
				if (des.length() != 0 && cant.length() != 0
						&& cant.matches(".*\\d.*")) {
					total();
				} else {
					// Sustituimos el valor del total si no se cumple
					total.setText("Total");
				}
			}
		});

		agregar.setOnClickListener(this);
		buscar.setOnClickListener(this);

		// Probando acceder a child row table
		fila.setOnClickListener(this);
		// textView.setOnClickListener(this);
		spinner1 = (Spinner) findViewById(R.id.Spinner1);
		addListenerOnSpinnerItemSelection();
		// //////////////////////////////////////////////////////////////////////////////////
	}

	// Opciones del menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.producto, menu);
		return true;
	}

	public void addListenerOnSpinnerItemSelection() {

		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener(
				ProductoActivity.this, check));
	}

	// Metodo que responde a los click de los botones
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.buttonBuscar:
			// Ejecutar asynctask
			new AsyncProducto(ProductoActivity.this, codigo.getText()
					.toString(), flag).execute();
			break;
		case R.id.ButtonAgregar:
			// Agregar fila a la bd
			Log.d("Insert: ", "Inserting ..");
			db.addProducto(new Producto(codigo.getText().toString(),
					descripcion.getText().toString(), Integer.parseInt(cantidad
							.getText().toString()), Double.parseDouble(total
							.getText().toString()), 12));

			// Relacionar tabla a variable y agregar filas

			Producto prod = db.getLastProducto();
			Log.d("Insert: ", Integer.toString(prod.getCantidad()));
			// grid = (TableLayout) findViewById(R.id.tabla_grid);
			grid = addRowToTable(grid, prod.getCodigo(), prod.getDescripcion(),
					prod.getCantidad(), prod.getTotal());

			break;
		case R.id.tableRow2:
			Toast.makeText(this, "LE HICE CLICK A LA TABLA", Toast.LENGTH_LONG)
					.show();
			break;

		}
	}

	// Metodo para sacar el total
	public void total() {
		cantidadTotal = Double.parseDouble(cantidad.getText().toString());
		descuentoTotal = Double.parseDouble(descuento.getText().toString());

		descuentoTotal = descuentoTotal / 100;
		Log.d("DESCUENTO %", Double.toString(descuentoTotal));

		precioVentaTotal = Integer.parseInt(precioVenta.getText().toString());
		// Formula de total
		totalT = cantidadTotal
				* (precioVentaTotal - (precioVentaTotal * descuentoTotal));
		// Redondeando
		totalT = Math.round(totalT * 100.0) / 100.0;
		String textTotal = Double.toString(totalT);
		total.setText(textTotal);

	}

	// /////////////////////////////////////////////////////////////////////////////////
	// Metodo para agregar filas a la tabla
	private TableLayout addRowToTable(TableLayout table, String codigoCol,
			String descripcionCol, int cantidadCol, double totalCol) {

		// Tabla parametros margenes
		@SuppressWarnings("deprecation")
		TableLayout.LayoutParams tableRowParams = new TableLayout.LayoutParams(
				TableLayout.LayoutParams.FILL_PARENT,
				TableLayout.LayoutParams.WRAP_CONTENT);

		int leftMargin = 2;
		int topMargin = 10;
		int rightMargin = 2;
		int bottomMargin = 2;

		tableRowParams.setMargins(leftMargin, topMargin, rightMargin,
				bottomMargin);
//////////////////////////////////////////////////////////////////////////////////////////
		
		// Inicializando algunas propiedades de la tabla
		table.setStretchAllColumns(true);
		table.setShrinkAllColumns(true);
		// Creando una fila para ir agregando las celdas
		TableRow rowProducto = new TableRow(this);
		TableRow rowTotal = new TableRow(this);
		TableRow rowLinea = new TableRow(this);
		// Fila vacia para dejar un margen
		TextView empty = new TextView(this);
		rowProducto.addView(empty);

		// celdas
		// Celda del codigo
		TextView codigoCelda = new TextView(this);
		codigoCelda.setText(codigoCol); // Celda del codigo
		codigoCelda.setTypeface(Typeface.DEFAULT_BOLD); // tipo de letra en la
														// vista

		rowProducto.addView(codigoCelda); // Agregando la celda a la fila

		TextView descripcionCelda = new TextView(this);
		descripcionCelda.setText(descripcionCol);
		descripcionCelda.setTypeface(Typeface.DEFAULT_BOLD);

		rowProducto.addView(descripcionCelda);

		TextView cantidadCelda = new TextView(this);
		String cantiCol = Integer.toString(cantidadCol);
		Log.d("CANTIDADCOLUMNA", cantiCol);
		cantidadCelda.setText(cantiCol);
		cantidadCelda.setTypeface(Typeface.DEFAULT_BOLD);

		rowProducto.addView(cantidadCelda);

		TextView totalCelda = new TextView(this);
		String totalCo = String.valueOf(totalCol);
		totalCelda.setText(totalCo);
		totalCelda.setTypeface(Typeface.DEFAULT_BOLD);

		rowProducto.addView(totalCelda);

		// Agregandole un listener a la fila creada
		rowProducto.setOnClickListener(clickRow);
		rowProducto.setBackgroundResource(drawable.row_selector);
		rowProducto.setLayoutParams(tableRowParams);
		table.addView(rowProducto); // Agregando la fila completa a la tabla

		// Agregando fila de total
		// Columna de unidades
		// Contando el total de filas actuales en la tabla
		int cantidadFilas = table.getChildCount();
		Log.d("CANTIDADDEFILAS", String.valueOf(cantidadFilas));

		// Si la tabla tiene mas de 3 habra que borrar la linea de la suma del
		// total
		if (cantidadFilas > 3) {
			table.removeViewAt(cantidadFilas - 2);
		}

		TextView empty2 = new TextView(this);
		empty2.setText("TOTALES:");
		empty2.setTypeface(Typeface.DEFAULT_BOLD);
		empty2.setTextColor(this.getResources().getColor(R.color.row));
		rowTotal.addView(empty2);
		TextView empty3 = new TextView(this);
		rowTotal.addView(empty3);
		TextView empty4 = new TextView(this);
		rowTotal.addView(empty4);
		TextView cantidadSumaTotal = new TextView(this);

		// String cantiCol = Integer.toString(cantidadCol);
		Log.d("CANTIDAD COLUMNA", cantiCol);
		cantidadSumaTotal.setText("SUPER TOTAL");
		// Hacer sumatoria de cantidad de unidades
		int suma = 0; // Inicializando variable que contendra total de unidades
		if (cantidadFilas == 3) { // Aqui se realiza la suma de una sola fila
			TableRow row1 = (TableRow) table.getChildAt(2);
			TextView view1 = (TextView) row1.getChildAt(3);
			// int suma1 = Integer.parseInt(view1.getText().toString());
			cantidadSumaTotal.setText(view1.getText().toString());
			cantidadSumaTotal.setTextColor(this.getResources().getColor(
					R.color.row));
		} else { // Suma mas de una fila
			for (int i = 2; i < cantidadFilas - 1; i++) {

				TableRow row = (TableRow) table.getChildAt(i);
				TextView view = (TextView) row.getChildAt(3);
				suma += Integer.parseInt(view.getText().toString());
				Log.d("CANTIDAD X", view.getText().toString());
				String sumaString = String.valueOf(suma);
				cantidadSumaTotal.setText(sumaString);
				cantidadSumaTotal.setTextColor(this.getResources().getColor(
						R.color.row));
			}
		}

		cantidadSumaTotal.setTypeface(Typeface.DEFAULT_BOLD);
		rowTotal.addView(cantidadSumaTotal);

		// Columna de totales
		TextView totalSumaTotal = new TextView(this);
		totalSumaTotal.setText("SUPER TOTAL");
		// Hacer sumatoria de cantidad de totales
		double suma2 = 0; // Inicializando variable que contendra total de
							// unidades
		if (cantidadFilas == 3) { // Aqui se realiza la suma de una sola fila
			TableRow row1 = (TableRow) table.getChildAt(2);
			TextView view1 = (TextView) row1.getChildAt(4);
			// int suma1 = Integer.parseInt(view1.getText().toString());
			totalSumaTotal.setText(view1.getText().toString());
			totalSumaTotal.setTextColor(this.getResources().getColor(
					R.color.row));
		} else { // Suma mas de una fila
			for (int i = 2; i < cantidadFilas - 1; i++) {

				TableRow row = (TableRow) table.getChildAt(i);
				TextView view = (TextView) row.getChildAt(4);
				suma2 += Double.parseDouble(view.getText().toString());
				suma2 = Math.round(suma2 * 100.0) / 100.0;
				Log.d("CANTIDAD X", view.getText().toString());
				String sumaString = String.valueOf(suma2);
				totalSumaTotal.setText(sumaString);
				totalSumaTotal.setTextColor(this.getResources().getColor(
						R.color.row));
			}
		}

		totalSumaTotal.setTypeface(Typeface.DEFAULT_BOLD);
		rowTotal.addView(totalSumaTotal);

		// anadiendo fila a la tabla
		table.addView(rowTotal);

		return table;
	}

	// On click listener para las filas de la tabla
	final OnClickListener clickRow = new OnClickListener() {
		public void onClick(final View v) {
			// Convertir la vista a rowview para acceder a sus celdas
			TableRow row = (TableRow) v;
			TextView view = (TextView) row.getChildAt(4);

			String valor = view.getText().toString();
			// Toast de prueba
			Toast.makeText(getApplicationContext(), valor, Toast.LENGTH_SHORT)
					.show();
		}
	};

}
