package com.bisa.posventa;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class TestingActivity2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_testing_activity2);

		// Boton de prueba
		Button boton = (Button) findViewById(R.id.button1);
		boton.setOnClickListener(onClickListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.testing_activity2, menu);
		return true;
	}

	// Captura los clicks en los botones
	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(final View v) {
			switch (v.getId()) {
			case R.id.button1:
				// DO something
				createTable();
				break;
			
			}
		}
	};
	
	// /////////////////////////////////////////////////////////////////////////
		// Encuentra una tabla y llama el metodo de agregar filas
		public void createTable() {
			Toast.makeText(getApplicationContext(), "probando", Toast.LENGTH_SHORT)
					.show();
			TableLayout grid = (TableLayout) findViewById(R.id.tabla);
			grid = addRowToTable(grid, "Hoursssssssssssssss", "Avg", "Romanos de asgard",
					getApplicationContext());

		}

		@SuppressWarnings("deprecation")
		private static TableLayout addRowToTable(TableLayout table,
				String contentCol1, String contentCol2, String contentCol3,
				Context context) {

			TableRow row = new TableRow(context);
			View line = new View(context);
			line.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, 1));
			line.setBackgroundColor(Color.rgb(2, 151, 51));
			// row.addView(line);

			TableRow.LayoutParams rowParams = new TableRow.LayoutParams();
			// Wrap-up the content of the row

			rowParams.height = LayoutParams.WRAP_CONTENT;
			rowParams.width = LayoutParams.WRAP_CONTENT;

			// The simplified version of the table of the picture above will have
			// two columns
			// FIRST COLUMN
			TableRow.LayoutParams col1Params = new TableRow.LayoutParams();
			// Wrap-up the content of the row
			col1Params.height = LayoutParams.WRAP_CONTENT;
			col1Params.width = LayoutParams.WRAP_CONTENT;
			// col1Params.span = 1;
			// Set the gravity to center the gravity of the column
			col1Params.gravity = Gravity.LEFT;
			col1Params.setMargins(5, 5, 5, 5);
			col1Params.weight = 3;
			TextView col1 = new TextView(context);
			col1.setText(contentCol1);

			col1.setTextColor(Color.parseColor("#000000"));
			row.addView(col1, col1Params);

			// SECOND COLUMN
			TableRow.LayoutParams col2Params = new TableRow.LayoutParams();
			// Wrap-up the content of the row
			col2Params.height = LayoutParams.WRAP_CONTENT;
			col2Params.width = LayoutParams.WRAP_CONTENT;
			// Set the gravity to center the gravity of the column
			col2Params.gravity = Gravity.LEFT;
			col2Params.setMargins(5, 5, 5, 5);
			col2Params.weight = 3;
			TextView col2 = new TextView(context);
			col2.setText(contentCol2);
			col2.setTextColor(Color.parseColor("#000000"));
			row.addView(col2, col2Params);

			// Tercer Columna
			TableRow.LayoutParams col3Params = new TableRow.LayoutParams();
			// Wrap-up the content of the row
			col3Params.height = LayoutParams.WRAP_CONTENT;
			col3Params.width = LayoutParams.WRAP_CONTENT;
			// Set the gravity to center the gravity of the column
			col3Params.gravity = Gravity.LEFT;
			col1Params.setMargins(5, 5, 5, 5);
			col1Params.weight = 3;
			TextView col3 = new TextView(context);
			col3.setText(contentCol3);
			col3.setTextColor(Color.parseColor("#000000"));
			row.addView(col3, col3Params);

			table.addView(row, rowParams);
//			table.setColumnStretchable(0, true);
//			table.setColumnStretchable(2, true);

			return table;
		}

}
