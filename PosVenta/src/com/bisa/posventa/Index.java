package com.bisa.posventa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bisa.adaptadores.TabsPagerAdapter;
import com.bisa.async.AsyncIndex;
import com.bisa.fragments.PedidosFragment;

public class Index extends FragmentActivity implements ActionBar.TabListener,
		android.view.View.OnClickListener {

	// Variables para uso global
	// Fragments contenedor, tabs y barra de accion
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	PedidosFragment pedidosFragment;
	
	

	// Tab titles
	private String[] tabs = { "Pedidos", "Cartera de Clientes" };
	// Spinner
	Spinner spinner;
	String[] valoresSpinner = { "Estado", "Cerrada", "Anulada", "Facturada" };
	List<String> spinnerValues = new ArrayList<String>();

	// DatePicker
	private int year;
	private int month;
	private int day;
	static final int DATE_PICKER_ID = 1111;

	// Elementos formulario
	TextView fecha;
	Button cambiarFecha;
	Button buscar;
	Button crear;
	EditText codigo;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_index);		
		
		// Apuntando a elementos del UI
		buscar = (Button) findViewById(R.id.IndexButtonBuscar);
		crear = (Button) findViewById(R.id.IndexButtonCrearPedido);
		codigo = (EditText) findViewById(R.id.IndexEditTextCliente);
		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		// Adaptador de spinner
		for (String valores : valoresSpinner) {
			spinnerValues.add(valores);
			Log.d("Spinner", valores);
		}

		spinner = (Spinner) findViewById(R.id.IndexSpinnerEstado);

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, spinnerValues);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);

		// Calendario
		// Get current date by calender

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);

		// Show current date
		fecha = (TextView) findViewById(R.id.IndexTextViewFecha);
		cambiarFecha = (Button) findViewById(R.id.IndexButtonFecha);
		fecha.setText("Fecha");

		// Button listener to show date picker dialog

		cambiarFecha.setOnClickListener(this);

		// Agregando listeners a botones
		buscar.setOnClickListener(this);
		crear.setOnClickListener(this);

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		

	}

	// Metodos datepicker
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:

			// open datepicker dialog.
			// set date picker for current date
			// add pickerListener listner to date picker
			return new DatePickerDialog(this, pickerListener, year, month, day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			year = selectedYear;
			month = selectedMonth;
			day = selectedDay;

			// Show selected date
			fecha.setText(new StringBuilder().append(day).append("-")
					.append(month + 1).append("-").append(year).append(" "));

		}
	};

	// Navegacion de tabs
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	// Creando menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.index, menu);
		return true;
	}

	// Metodo que detecta los click
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.IndexButtonBuscar:
			//Ejecutar asynctask
			new AsyncIndex(this).execute();
			//Toast.makeText(this, "LE HICE CLICK A LA TABLA", Toast.LENGTH_LONG)
			//.show();
			break;
		case R.id.IndexButtonCrearPedido:
			Intent pedidos = new Intent (this, ProductoActivity.class);
			String codigoCliente = codigo.getText().toString();
			pedidos.putExtra("Codigo", codigoCliente);
			startActivity(pedidos);
			break;
		case R.id.IndexButtonFecha:
			showDialog(DATE_PICKER_ID);
			break;
		}

	}

}
