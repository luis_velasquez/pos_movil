package com.bisa.posventa;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class TestingActivity extends Activity {

	// we declare the layout and the screen container as globals for the class
	ScrollView sv;
	TableLayout myTableLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_testing);

		// lets initialize the screen containers
		sv = new ScrollView(this);
		myTableLayout = new TableLayout(this);

		drawScreen();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.testing, menu);
		return true;
	}

	// Metdos//////////////////////////

	// Metodo para dibujar los elementos en la pantalla
	public void drawScreen() {
		// this might be a redraw, so we clean the view's container
		myTableLayout.removeAllViews();
		sv.removeAllViews();

		// special rows
		TableRow buttonRow = new TableRow(this);
		TableRow titleRow = new TableRow(this);

		// margins
		buttonRow.setPadding(20, 10, 20, 10);
		titleRow.setPadding(20, 10, 20, 10);

		// the title
		TextView title = new TextView(this);
		title.setText("GRID");
		title.setTextSize(14);
		title.setTextColor(Color.BLACK);
		title.setGravity(Gravity.CENTER_HORIZONTAL);

		titleRow.addView(title);
		titleRow.setGravity(Gravity.CENTER);

		// the title tablelayout
		TableLayout titleTableLayout = new TableLayout(this);
		titleTableLayout.addView(titleRow);

		// we add the title layout to the main one <img
		// src='http://forum.codecall.net/public/style_emoticons/<#EMO_DIR#>/wink.png'
		// class='bbc_emoticon' alt=';)' />
		myTableLayout.addView(titleTableLayout);

		// / the 3 input rows
		inputRow2(myTableLayout, "Columna1", "Columna2", "Columna3");
		
		//

		// the buttons table layout
		// purpose : right align for both buttons
		TableLayout buttonsLayout = new TableLayout(this);
		buttonRow.setPadding(80, 50, 40, 0);

		// the accept and cancel buttons
		Button btAccept = new Button(this);
		btAccept.setText("Accept");
		Button btCancel = new Button(this);
		btCancel.setText("Cancel");

		buttonRow.addView(btAccept);
		buttonRow.addView(btCancel);
		buttonRow.setGravity(Gravity.RIGHT);

		buttonsLayout.addView(buttonRow);

		myTableLayout.addView(buttonsLayout);
		sv.addView(myTableLayout);
        
//		RelativeLayout layout = (RelativeLayout) findViewById(R.layout.activity_testing);
//		layout.addView(sv);
		// set the screen content to table layout's
		setContentView(sv);
	}

	//
	// inputRow :
	// adds a label with an inputtextbox as a tablerow to a an existing
	// tablelayout
	//
	// Tablelayout : layout to fill
	// label : input's label
	// inputsize : in character, for drawing an aprox size
	// inputID : IMPORTANT ! you will use that id to get the input value with a
	// getID
	// taking into account that you will do the validation at the end (by
	// looping the IDS)
	// or implementing an unique listener which will receive the editText as a
	// parameter
	//
	public void inputRow(TableLayout tl, String label, int inputSize,
			int inputID) {
		TableRow inputRow = new TableRow(this);
		TextView tv = new TextView(this);
		EditText edit = new EditText(this);

		// some margin
		inputRow.setPadding(20, 10, 20, 0);
		tv.setText(label);
		edit.setMaxWidth(inputSize * 7);
		edit.setMinimumWidth(inputSize * 7);
		edit.setId(inputID);
		edit.setGravity(Gravity.RIGHT);
		inputRow.addView(tv);
		inputRow.addView(edit);

		tl.addView(inputRow);
	}

	public void inputRow2(TableLayout t1, String col1, String col2, String col3) {

		TableRow inputRow = new TableRow(this);
		TextView columna1 = new TextView(this);
		TextView columna2 = new TextView(this);
		TextView columna3 = new TextView(this);

		// some margin
		inputRow.setPadding(60, 10, 20, 0);
		columna1.setText(col1); 
		columna1.setPadding(5, 5, 15, 5);
		columna2.setText(col2);
		columna2.setPadding(5, 5, 15, 5);
		columna3.setText(col3);
		columna3.setPadding(5, 5, 15, 5);
		
		inputRow.addView(columna1);
		inputRow.addView(columna2);
		inputRow.addView(columna3);	
		
		t1.addView(inputRow);

	}
	
	
}
