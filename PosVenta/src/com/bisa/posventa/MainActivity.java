package com.bisa.posventa;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Boton de prueba
		Button boton = (Button) findViewById(R.id.button1);
		boton.setOnClickListener(onClickListener);

		// Ir a otra actividad
		Button boton1 = (Button) findViewById(R.id.button2);
		boton1.setOnClickListener(onClickListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Metodos

	// Captura los clicks en los botones
	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(final View v) {
			switch (v.getId()) {
			case R.id.button1:
				// DO something
				createTable();
				break;
			case R.id.button2:
				// DO something
				otraActividad();
				break;

			}
		}
	};
	
	//Ir a otra actividad
	public void otraActividad(){
		Intent activity = new Intent (getApplicationContext(), TestingActivity.class);
		startActivity(activity);
	}

	// /////////////////////////////////////////////////////////////////////////
	// Encuentra una tabla y llama el metodo de agregar filas
	public void createTable() {
		Toast.makeText(getApplicationContext(), "probando", Toast.LENGTH_SHORT)
				.show();
		TableLayout grid = (TableLayout) findViewById(R.id.grid);
		grid = addRowToTable(grid, "Hour", "Avg", "Romanos de asgard",
				getApplicationContext());

	}

	@SuppressWarnings("deprecation")
	private static TableLayout addRowToTable(TableLayout table,
			String contentCol1, String contentCol2, String contentCol3,
			Context context) {

		TableRow row = new TableRow(context);
		View line = new View(context);
		line.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, 1));
		line.setBackgroundColor(Color.rgb(2, 51, 51));
		// row.addView(line);

		TableRow.LayoutParams rowParams = new TableRow.LayoutParams();
		// Wrap-up the content of the row

		rowParams.height = LayoutParams.WRAP_CONTENT;
		rowParams.width = LayoutParams.WRAP_CONTENT;

		// The simplified version of the table of the picture above will have
		// two columns
		// FIRST COLUMN
		TableRow.LayoutParams col1Params = new TableRow.LayoutParams();
		// Wrap-up the content of the row
		col1Params.height = LayoutParams.WRAP_CONTENT;
		col1Params.width = LayoutParams.WRAP_CONTENT;
		// col1Params.span = 1;
		// Set the gravity to center the gravity of the column
		col1Params.gravity = Gravity.CENTER;
		col1Params.setMargins(5, 5, 5, 5);
		TextView col1 = new TextView(context);
		col1.setText(contentCol1);

		col1.setTextColor(Color.parseColor("#000000"));
		row.addView(col1, col1Params);

		// SECOND COLUMN
		TableRow.LayoutParams col2Params = new TableRow.LayoutParams();
		// Wrap-up the content of the row
		col2Params.height = LayoutParams.WRAP_CONTENT;
		col2Params.width = LayoutParams.WRAP_CONTENT;
		// Set the gravity to center the gravity of the column
		col2Params.gravity = Gravity.CENTER;
		col2Params.setMargins(5, 5, 5, 5);
		TextView col2 = new TextView(context);
		col2.setText(contentCol2);
		col2.setTextColor(Color.parseColor("#000000"));
		row.addView(col2, col2Params);

		// Tercer Columna
		TableRow.LayoutParams col3Params = new TableRow.LayoutParams();
		// Wrap-up the content of the row
		col3Params.height = LayoutParams.WRAP_CONTENT;
		col3Params.width = LayoutParams.WRAP_CONTENT;
		// Set the gravity to center the gravity of the column
		col3Params.gravity = Gravity.CENTER;
		col1Params.setMargins(5, 5, 5, 5);
		TextView col3 = new TextView(context);
		col3.setText(contentCol3);
		col3.setTextColor(Color.parseColor("#000000"));
		row.addView(col3, col3Params);

		table.addView(row, rowParams);

		return table;
	}

}
