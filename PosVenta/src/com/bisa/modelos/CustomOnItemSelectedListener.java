package com.bisa.modelos;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

import com.bisa.async.AsyncProducto;
import com.bisa.posventa.ProductoActivity;
import com.bisa.posventa.R;

public class CustomOnItemSelectedListener implements OnItemSelectedListener {

	//Inicializando variables globales
	ProductoActivity contexto;
	String flag = "nada";
	EditText codigo;
	int check;

	public CustomOnItemSelectedListener(ProductoActivity contexto, int check) {
		// TODO Auto-generated constructor stub
		this.contexto = contexto;

		this.check = check;
	}

	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		//Nombre de la unidad de medida seleccionada
		String nombre = parent.getItemAtPosition(pos).toString();
		//Aqui me aseguro que si el spinner esta en posicion 0 no hara peticion a la BD
		check = check + 1;
		if (check > 1 && pos > 0) {
			//Capturo el valor del codigo
			codigo = (EditText) contexto.findViewById(R.id.editTextCodigo);

//			Toast.makeText(
//					parent.getContext(),
//					"On Item Select : \n"
//							+ nombre + pos
//							+ "codigo: " +codigo.getText().toString(), Toast.LENGTH_LONG).show();
			//LLamo a la base para traer nuevo precio de venta
			 new AsyncProducto(contexto, codigo.getText().toString(), flag, nombre, pos).execute();
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}